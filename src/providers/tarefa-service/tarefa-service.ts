import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

import { Tarefa } from './../../pages/tarefas/Tarefa';
import { tarefasPadrao } from './../../pages/tarefas/tarefasPadrao'

@Injectable()
export class TarefaServiceProvider {

  tarefas: Tarefa[] = [];

  constructor(private _storage: Storage) {
    this.carregarTarefas();
  }

  /**
   * Carrega as tarefas do storage
   * e monta o vetor a ser utilizado.
   * Se não há nada no storage, monta o
   * vetor através de tarefas padrão.
   *
   * Perceba que .get retorna uma Promise.
   */
  private carregarTarefas() {

    this._storage.get('tarefas')
      .then((tarefas) => {

        if (!!tarefas && tarefas.length > 0) {
          console.log('Obtendo tarefas de IndexedDB');
          this.mapearTarefas(tarefas);
        }
        else {
          console.log('Obtendo tarefas padrão');
          this.mapearTarefas(tarefasPadrao);
        }
      });
  }

  /**
   * Mantém o storage atualizado
   */
  private persistirStorage() {
    this._storage.set('tarefas', this.tarefas);
  }

  /**
   * Para cada tarefa, é feita a criação
   * de um novo objeto do tipo Tarefa e
   * inserido no vetor de tarefas disponibilizado
   * ao desenvolvedor
   * @param tarefasMap
   */
  private mapearTarefas(tarefasMap) {
    tarefasMap.forEach(tarefa => {
      this.tarefas.push(
        new Tarefa(
          tarefa._nome,
          tarefa._descricao,
          tarefa._estado,
          tarefa._dataCriacao,
        )
      );
    });
  }

  /**
   * Retorna os dados atualizados
   * às páginas que utilizam o
   * serviço
   */
  public obterTarefas() {
    return this.tarefas;
  }

  /**
   * Retira a tarefa a ser excluída
   * da lista e em seguida atualiza
   * a lista
   * @param tarefa
   */

  public excluir(tarefa: Tarefa) {

    const newTarefas = this.tarefas.filter(
      (tarefaDaVez) => {
        return (tarefa.nome !== tarefaDaVez.nome)
      }
    );

    this.tarefas = newTarefas;
    this.persistirStorage();
  }

  /**
   * Cria uma nova tarefa a partir
   * da inserção do novo objeto no vetor
   * de tarefas
   * @param nome
   * @param descricao
   */
  public nova(nome: string, descricao: string) {
    this.tarefas.push(new Tarefa(nome, descricao));
    this.persistirStorage();
  }

  /**
   * Para cada tarefa, verifica a tarefa desejada
   * pelo nome e a marca como concluída
   * @param tarefaAConcluir
   */
  public concluir(tarefaAConcluir: Tarefa) {
    for (const tarefa of this.tarefas)
      if (tarefa.nome === tarefaAConcluir.nome)
        tarefa.concluir();

    this.persistirStorage();
  }
}
