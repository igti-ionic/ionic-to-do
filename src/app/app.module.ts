import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Storage, StorageConfig } from '@ionic/storage';

import { IonicToDo } from './app.component';

/**
 * Página que foi criada e importada ao módulo,
 * incluída na declaração e entryComponents
 */
import { TarefasPage } from '../pages/tarefas/tarefas';
import { TarefaServiceProvider } from '../providers/tarefa-service/tarefa-service';

/**
 * Objeto de configuração de Storage
 */
const storageConfig: StorageConfig = {
  name: 'indexeddb',
  storeName: 'tarefas',
  driverOrder: ['indexeddb']
};

/**
 * Função que retorna uma
 * instância de Storage
 */
function provideStorage() {
  return new Storage(storageConfig);
}

@NgModule({
  declarations: [
    IonicToDo,

    /**
     * Incluímos apenas TarefasPage
     * nas declarações e entryComponents
     */
    TarefasPage
  ],
  imports: [BrowserModule, IonicModule.forRoot(IonicToDo)],
  bootstrap: [IonicApp],
  entryComponents: [IonicToDo, TarefasPage],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },

    /**
     * Ajudamos o Ionic a instanciar Storage através de useFactory
     */
    { provide: Storage, useFactory: provideStorage },

    /**
     * Como é uma classe simples, o Ionic sabe instanciar
     */
    TarefaServiceProvider
  ]
})
export class AppModule {}
