import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

/**
 * Página que foi criada e importada ao componente
 */
import { TarefasPage } from './../pages/tarefas/tarefas';

@Component({
  templateUrl: 'app.html'
})
export class IonicToDo {

  /**
   * Definindo TarefasPage como
   * página inicial do app
   */
  rootPage: any = TarefasPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

