/**
 * Objeto json com tarefas padrão
 */
export const tarefasPadrao = [
  {
    _nome: 'Leite',
    _descricao: 'Comprar leite no supermercado',
    _estado: 'concluida',
    _dataCriacao: new Date(2018, 1, 1, 5, 5, 5),
  },
  {
    _nome: 'Futebol',
    _descricao: 'Jogar futebol na quinta-feira',
    _estado: 'nova',
    _dataCriacao: new Date(2018, 2, 1, 5, 6, 7),
  },
  {
    _nome: 'Estudar Ionic',
    _descricao: 'Estudar para as aulas do IGTI',
    _estado: 'concluida',
    _dataCriacao: new Date(2018, 1, 1, 5, 5, 5),
  },
  {
    _nome: 'Cinema',
    _descricao: 'Assistir a Star Wars',
    _estado: 'nova',
    _dataCriacao: new Date(2018, 4, 1, 11, 56, 53),
  },
  {
    _nome: 'Video game',
    _descricao: 'Terminar Horizon - Zero Dawn',
    _estado: 'concluida',
    _dataCriacao: new Date(2018, 1, 1, 5, 5, 5),
  },
  {
    _nome: 'Correr',
    _descricao: 'Correr 5 km',
    _estado: 'nova',
    _dataCriacao: new Date(2019, 7, 8, 23, 4, 22),
  },
];
