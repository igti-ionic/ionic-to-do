/**
 * Classe de negócio
 */
export class Tarefa {

  /**
   * Atributos
   */
  private _nome: string;
  private _descricao: string;
  private _dataCriacao: Date;
  private _estado: string;

  /**
   * Descrição, estado da tarefa
   * e data de criação são parâmetros
   * opcionais, pois possuem um
   * valor default
   * @param pNome
   * @param pDescricao
   * @param pEstado
   * @param pDataCriacao
   */
  constructor(
    pNome: string,
    pDescricao: string = '',
    pEstado: string = 'nova',
    pDataCriacao: Date = new Date()) {

    /**
     * Inicialização
     */
    this._nome = pNome;
    this._descricao = pDescricao;
    this._estado = pEstado
    this._dataCriacao = pDataCriacao;
  }

  /**
   * Getters
   */
  get nome() {
    return this._nome;
  }

  get descricao() {
    return this._descricao;
  }

  get dataCriacao() {
    return this._dataCriacao;
  }

  get estado() {
    return this._estado;
  }

  /**
   * Verificando se a tarefa
   * está concluída
   */
  isConcluida(): boolean {
    return this._estado === 'concluida';
  }

  /**
   * Conclusão da tarefa
   */
  concluir() {
    this._estado = 'concluida';
  }
}
