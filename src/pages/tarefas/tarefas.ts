import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController
} from 'ionic-angular';

import { Tarefa } from './Tarefa';
import { ItemSliding } from 'ionic-angular/components/item/item-sliding';
import { TarefaServiceProvider } from '../../providers/tarefa-service/tarefa-service';

@IonicPage()
@Component({
  selector: 'page-tarefas',
  templateUrl: 'tarefas.html'
})
export class TarefasPage {
  /**
   * Vetor de tarefas inicializado
   * como vazio
   */
  tarefas: Tarefa[] = [];

  /**
   * Construtor com 4 injeções de dependência
   * @param navCtrl navigation controller
   * @param navParams navigation params
   * @param alertCtrl alert controller
   * @param tarefaService serviço de tarefas
   */
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private tarefaService: TarefaServiceProvider
  ) {
    this.sincronizar();
  }

  /**
   * Sincroniza os dados com o storage.
   * O Ionic faz o restante do trabalho, que é a
   * reconciliação dos dados com a interface
   * (reatividade)
   */
  private sincronizar() {
    this.tarefas = this.tarefaService.obterTarefas();
  }

  /**
   * Invocando navController para
   * chamar a página de detalhes da tarefa,
   * enviando a tarefa a ser detalhada
   * como parâmetro
   * @param tarefa
   */
  detalheTarefa(tarefa: Tarefa) {
    this.navCtrl.push('DetalheTarefaPage', {
      tarefaDetalhe: tarefa
    });
  }

  /**
   * Marcando tarefa como concluída
   * @param slidingItem
   * @param tarefa
   */
  concluir(slidingItem: ItemSliding, tarefa: Tarefa) {
    this.tarefaService.concluir(tarefa);
    this.sincronizar();

    /**
     * Fechando o slide
     * programaticamente
     */
    slidingItem.close();
  }

  /**
   * Exclusão, através de filtragem
   * @param tarefa
   */
  excluir(tarefa: Tarefa) {
    this.tarefaService.excluir(tarefa);
    this.sincronizar();
  }

  /**
   * Nova tarefa, através de um alertController
   */
  novaTarefa() {
    let alert = this.alertCtrl.create({
      title: 'Nova tarefa',
      inputs: [
        {
          name: 'nome',
          placeholder: 'Nome da tarefa'
        },
        {
          name: 'descricao',
          placeholder: 'Descrição da tarefa'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel'
        },
        {
          text: 'Ok',
          handler: data => {
            if (data.nome.trim() !== '') {
              this.tarefaService.nova(data.nome, data.descricao);
              this.sincronizar();
            }
          }
        }
      ]
    });
    alert.present();
  }
}
