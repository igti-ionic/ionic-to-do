import { Tarefa } from './../tarefas/Tarefa';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-detalhe-tarefa',
  templateUrl: 'detalhe-tarefa.html',
})
export class DetalheTarefaPage {

  /**
   * Tarefa a ser detalhada
   */
  tarefaDetalhe: Tarefa;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    /**
     * Obtendo a tarefa através dos parâmetros
     * de navegação
     */
    this.tarefaDetalhe =
      this.navParams.get('tarefaDetalhe');
  }
}
